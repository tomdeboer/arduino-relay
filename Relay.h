#ifndef __Relay_H__
#define __Relay_H__

#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif


#define RELAY_NORMAL false
#define RELAY_REVERSED true


class Relay {
	public:
		Relay( uint8_t pin, bool mode );

		void turnOn();
		void turnOff();
		bool isOn();
		bool isOff();
		
		void setMode( bool );
	private:

	protected:
		bool _on; // Defined what on is ( HIGH or LOW ). Is used for comparison in case of a reversed relay.
		uint8_t _pin; // Relay pin
};
#endif
