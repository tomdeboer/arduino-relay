#include "Relay.h"

Relay::Relay( uint8_t pin, bool mode ) {
	
	setMode( mode );

	_pin = pin;

	pinMode( pin , OUTPUT );
	digitalWrite( _pin, not _on );
	
}

void Relay::turnOn() {
	digitalWrite(_pin, _on );
}

void Relay::turnOff() {
	digitalWrite(_pin, not _on );
}

bool Relay::isOff() {
	return digitalRead( _pin ) == not _on;
}

bool Relay::isOn() {
	return digitalRead( _pin ) == _on;
}

void Relay::setMode( bool mode ){
	_on = mode == RELAY_NORMAL ? HIGH : LOW;
}
